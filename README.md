# Allium <!-- omit in toc -->

Scalliony Web API and UI

- [About The Project](#about-the-project)
- [Built with](#built-with)
- [Use it](#use-it)
- [Run it](#run-it)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Docker](#docker)
  - [Log level](#log-level)
  - [Tests](#tests)
- [License](#license)

<!-- ABOUT THE PROJECT -->
## About The Project

Opensource RTS programming game using WebAssembly

This project contains web server hosting API and UI

## Built with

* NodeJs
* uWebSockets
* TypeScript
* PostgreSQL
* Love and insomnia

## Use it

Visit https://dev.scalliony.wadza.fr or use a standalone client querying /api

## Run it

### Prerequisites

* Node + Yarn
* Access to [chives](https://framagit.org/scalliony/chives) database

### Installation

1. Clone the project repo
```sh
git clone --recursive https://framagit.org/scalliony/allium.git
```

Server

2. Setup modules
```sh
yarn install
```
3. Compile
```sh
yarn run build
```

Client

4. Setup modules
```sh
cd client
yarn install
```
5. Compile
```sh
yarn run build
cd ../build
mv ../client/dist ./client
```

### Usage

In `build` folder
```sh
NODE_ENV=production DB_URI=<postgresql://...> node index.js
```

### Docker

From DockerHub
```sh
docker run scalliony/allium -e DB_URI='<postgresql://...>'
```
[Dockerfile](Dockerfile) simply bundles following steps with a multi-stage image.
*No need to manually clone the full repo, copy just this Dockerfile*
```sh
docker build -t custom-allium --build-arg COMMIT=<SHA> - < Dockerfile
docker run custom-allium
```

### Log level

This project logging is similar to [env_logger](https://docs.rs/env_logger), log level can be updated with `LOG_LEVEL` env variable

### Tests
```
yarn run test
```

<!-- LICENSE -->
## License

Distributed under the MIT License. See [LICENSE](LICENSE) for more information.