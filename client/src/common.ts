export enum QueryKind {
    Move = 'move',
    Entity = 'entity'
}
export interface QueryModel<K extends QueryKind, T> {
    kind: K
    data: T
}
export type Query =
    QueryModel<QueryKind.Move, Area> |
    QueryModel<QueryKind.Entity, string>

export interface Cell {
    x: number
    y: number
}
export function RoundCell({ x, y }: Cell) {
    return { x: Math.round(x), y: Math.round(y) }
}
export interface Area {
    min: Cell
    max: Cell
}
export function RoundArea({ min, max }: Area) {
    return { min: RoundCell(min), max: RoundCell(max) }
}

export enum EntityType {
    Bot = 'bot',
    Rock = 'rock',
    Building = 'building'
}

export enum EventType {
    Tick = 'tick',
    Move = 'move',
    Rotate = 'rotate',
    Log = 'log',
    Protocol = 'protocol',
    Entities = 'entities'
}
export enum Rotation { Up, Right, Down, Left }
export function toRad(r: Rotation) {
    switch (r) {
        case Rotation.Up:
            return 0
        case Rotation.Right:
            return Math.PI / 2
        case Rotation.Down:
            return Math.PI
        case Rotation.Left:
            return Math.PI * 3 / 2
    }
}

interface EventModel<T extends EventType, D> {
    id: string
    type: T
    data: D
}
interface EntityData {
    id: string
}
export interface FullEntity extends EntityData {
    type: EntityType
    position: Cell
    rotation?: Rotation
}

export type Event =
    EventModel<EventType.Tick, never> |
    EventModel<EventType.Move, EntityData & { position: Cell }> |
    EventModel<EventType.Rotate, EntityData & { rotation: Rotation }> |
    EventModel<EventType.Entities, FullEntity[]> |
    EventModel<EventType.Protocol, any>

export type AnyEvent = EventModel<EventType, object>