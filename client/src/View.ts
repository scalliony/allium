import { Area, Cell } from "./common"

const CELL_SIZE =  128

class Canvas {
    private el: HTMLCanvasElement
    private ctx: CanvasRenderingContext2D
    private size: Cell = { x: 0, y: 0 }

    constructor(container: HTMLElement, zIndex: number) {
        this.el = document.createElement('canvas')
        this.el.style.position = 'absolute'
        this.el.style.zIndex = zIndex.toString()
        container.appendChild(this.el)
        const resize = () => {
            this.size = { x: container.offsetWidth, y: container.offsetHeight }
            this.el.width = this.size.x
            this.el.height = this.size.y
        }
        window.addEventListener('resize', () => resize())
        resize()

        this.ctx = this.el.getContext('2d')!
    }

    public clear() {
        this.ctx.clearRect(0, 0, this.size.x, this.size.y)
    }

    public drawBot(at: Cell, scale: number, rotate: number) {
        const size = CELL_SIZE * scale
        this.ctx.save();
        this.ctx.translate(Math.floor(at.x + size / 2), Math.floor(at.y + size / 2))
        this.ctx.rotate(rotate)
        this.ctx.fillStyle = 'red'
        this.ctx.beginPath()
        this.ctx.arc(0, 0, size / 2, 0, 2 * Math.PI)
        this.ctx.fill()
        this.ctx.strokeStyle = 'yellow'
        this.ctx.beginPath()
        this.ctx.moveTo(0, -size / 2)
        this.ctx.lineTo(0, 0)
        this.ctx.stroke()
        this.ctx.restore()
    }
    public drawRock(at: Cell, scale: number) {
        const size = CELL_SIZE * scale
        const off = size / 10
        this.ctx.fillStyle = 'grey'
        this.ctx.fillRect(Math.floor(at.x + off / 2), Math.floor(at.y + off / 2),
            Math.floor(size - off), Math.floor(size - off))
    }

    public drawGrid(offset: Cell, scale: number) {
        const cell = CELL_SIZE * scale
        this.ctx.beginPath()
        for (let x = offset.x; x < this.size.x; x += cell) {
            const X = Math.floor(x)
            this.ctx.moveTo(X, 0)
            this.ctx.lineTo(X, this.size.y)
        }
        for (let y = offset.y; y < this.size.y; y += cell) {
            const Y = Math.floor(y)
            this.ctx.moveTo(0, Y)
            this.ctx.lineTo(this.size.x, Y)
        }
        this.ctx.closePath()
        this.ctx.strokeStyle = 'black'
        this.ctx.lineWidth = 1
        this.ctx.stroke()
    }
}

export default class View {
    public options = {
        show_grid: true,
        tick_per_second: 2
    }

    public onFrame: (delta: number, resized: boolean) => void = () => { }
    private lastTime = 0
    private resized = false

    private container: HTMLElement
    private staticLayer: Canvas
    private dynamicLayer: Canvas
    private uiLayer: Canvas

    constructor(containerId: string) {
        this.container = document.getElementById(containerId)!
        this.updatePosition()

        this.staticLayer = new Canvas(this.container, 1)
        this.dynamicLayer = new Canvas(this.container, 2)
        this.uiLayer = new Canvas(this.container, 3)
        window.addEventListener('resize', () => {
            this.updatePosition()
        })

        //TODO: click and drag
        this.container.addEventListener('wheel', (e) => {
            e.preventDefault()
            const ZOOM_SPEED = 1.1
            this.scale = Math.max(.1, Math.min(4, e.deltaY > 0 ? this.scale * ZOOM_SPEED : this.scale / ZOOM_SPEED))
        })

        requestAnimationFrame(time => this.doFrame(time))
    }

    /** Position of screen center in game space */
    private centerPosition: Cell = { x: 0, y: 0 }
    /** Position of screen top in game space */
    private topPosition: Cell = { x: 0, y: 0 }
    private viewScale = 1

    private updatePosition() {
        this.topPosition = {
            x: this.centerPosition.x * CELL_SIZE * this.viewScale - this.container.offsetWidth / 2,
            y: this.centerPosition.y * -CELL_SIZE * this.viewScale - this.container.offsetHeight / 2
        }
        this.resized = true
    }
    public get position() { return this.centerPosition }
    public set position(value: Cell) {
        this.centerPosition = value
        this.updatePosition()
    }
    public get scale() { return this.viewScale }
    public set scale(value: number) {
        this.viewScale = value
        this.updatePosition()
    }

    public area(ratio: number): Area {
        const size = {
            x: this.container.offsetWidth / CELL_SIZE / this.viewScale * ratio / 2,
            y: this.container.offsetHeight / CELL_SIZE / this.viewScale * ratio / 2
        }
        return {
            min: {
                x: this.centerPosition.x - size.x,
                y: this.centerPosition.y - size.y,
            }, max: {
                x: this.centerPosition.x + size.x,
                y: this.centerPosition.y + size.y,
            }
        }
    }

    private doFrame(time: number) {
        this.dynamicLayer.clear()
        this.onFrame(time - this.lastTime, this.resized)
        this.lastTime = time
        this.resized = false
        requestAnimationFrame(time => this.doFrame(time))
    }

    public drawBot(at: Cell, rotate: number = 0, dynamic: boolean = true) {
        const layer = dynamic ? this.dynamicLayer : this.staticLayer
        layer.drawBot({
            x: at.x * CELL_SIZE * this.scale - this.topPosition.x,
            y: at.y * -CELL_SIZE * this.scale - this.topPosition.y
        }, this.scale, rotate)
    }
    public drawRock(at: Cell, dynamic: boolean = true) {
        const layer = dynamic ? this.dynamicLayer : this.staticLayer
        layer.drawRock({
            x: at.x * CELL_SIZE * this.scale - this.topPosition.x,
            y: at.y * -CELL_SIZE * this.scale - this.topPosition.y
        }, this.scale)
    }

    public clearStatic() {
        this.staticLayer.clear()
        if (this.options.show_grid)
            this.staticLayer.drawGrid({
                x: -this.topPosition.x % (CELL_SIZE * this.scale),
                y: -this.topPosition.y % (CELL_SIZE * this.scale)
            }, this.scale)
    }
}