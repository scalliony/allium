import Game from './Game'
import "./style.sass"

document.addEventListener("DOMContentLoaded", () => {
    new Game()
})