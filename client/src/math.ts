import { Cell } from "./common";

export function lerp(from: number, to: number, progress: number) {
    return (1 - progress) * from + progress * to;
}
export function lerp2d(from: Cell, to: Cell, progress: number) {
    return { x: lerp(from.x, to.x, progress), y: lerp(from.y, to.y, progress) }
}

function shortRadDist(from: number, to: number) {
    const max = Math.PI * 2
    const diff = (to - from) % max
    return ((2 * diff) % max) - diff
}
export function lerpRad(from: number, to: number, progress: number) {
    return from + shortRadDist(from, to) * progress
}

export function manhattan(from: number, to: number) {
    return Math.abs(from - to)
}
export function manhattan2d(from: Cell, to: Cell) {
    return manhattan(from.x, to.x) + manhattan(from.y, to.y)
}