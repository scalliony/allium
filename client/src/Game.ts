import Stream from './Stream'
import { Cell, EntityType, Event, EventType, FullEntity, Rotation, toRad } from './common'
import View from './View'
import { lerp2d, lerpRad } from './math'

class LerpProp<T> {
    private to: T
    private from?: T
    private progress?: number

    constructor(at: T, private readonly lerp: (from: T, to: T, progress: number) => T) {
        this.to = at
    }

    public get done(): boolean {
        return this.from === undefined
    }
    public get current() {
        return this.done ? this.to :
            this.lerp(this.from!, this.to, this.progress!)
    }

    public step(step: number) {
        if (!this.done) {
            this.progress! += step
            if (this.progress! >= 1) {
                this.from = undefined
                this.progress = undefined
            }
        }
    }

    public set(to: T) {
        this.from = this.current
        this.progress = 0
        this.to = to
    }
}
class Entity {
    public readonly type: EntityType
    private pos: LerpProp<Cell>
    private rotation?: LerpProp<number>
    private animations: {
        last: number
        static: boolean
    }

    constructor(type: EntityType, pos: Cell, rot?: Rotation) {
        this.type = type
        this.pos = new LerpProp<Cell>(pos, lerp2d)
        if (rot !== undefined && rot !== null)
            this.rotation = new LerpProp<number>(toRad(rot), lerpRad)
        this.animations = { static: false, last: 0 }
    }

    public stepAnimations(step: number) {
        this.pos.step(step)
        this.rotation?.step(step * 3)
    }

    public get animatedPos() { return this.pos.current }
    public get animatedRot() { return this.rotation?.current ?? 0 }

    private animate() {
        this.animations.last = Date.now()
        return this.animations.static
    }

    public animatePos(to: Cell) {
        this.pos.set(to)
        return this.animate()
    }
    public animateRot(to: Rotation) {
        if (this.rotation) {
            this.rotation.set(toRad(to))
            return this.animate()
        }
        return false
    }

    public freeze(freezeTime: number) {
        this.animations.static = this.animations.last < freezeTime &&
            this.pos.done && (this.rotation?.done ?? true)
        return this.freezed
    }
    public get freezed() { return this.animations.static }

    public draw(view: View, dyn: boolean) {
        switch (this.type) {
            case EntityType.Rock:
                view.drawRock(this.animatedPos, dyn)
                break
            case EntityType.Bot:
                view.drawBot(this.animatedPos, this.animatedRot, dyn)
                break
        }
    }
}
interface EntityQuery {
    retry?: NodeJS.Timeout, fail: NodeJS.Timeout,
    resolve: ((e: Entity) => void)[], reject: ((err: string) => void)[]
}


export default class Game {

    private view: View
    private entities = new Map<string, Entity>()
    private pendingEntities = new Map<string, EntityQuery>()
    private needFullRender: boolean = true

    private pressedKeys: { [key: string]: boolean } = {};
    private stream: Stream

    constructor() {
        this.stream = new Stream(this.eventListener.bind(this))

        this.view = new View('app')

        window.addEventListener("keydown", this.keyboardListener.bind(this))
        window.addEventListener("keyup", this.keyboardListener.bind(this))

        this.view.onFrame = this.onFrame.bind(this)
    }

    private onFrame(deltaTime: number, moved: boolean) {
        // Handle keyboard
        const MOVES = [
            { keys: ['ArrowUp', 'KeyW'], move: { x: 0, y: 1 } },
            { keys: ['ArrowRight', 'KeyD'], move: { x: 1, y: 0 } },
            { keys: ['ArrowDown', 'KeyS'], move: { x: 0, y: -1 } },
            { keys: ['ArrowLeft', 'KeyA'], move: { x: -1, y: 0 } }
        ]
        const moveDelta = (deltaTime / 200) / this.view.scale
        for (const {keys, move} of MOVES) {
            for (const key of keys) {
                if (this.pressedKeys[key]) {
                    this.view.position = {
                        x: this.view.position.x + move.x * moveDelta,
                        y: this.view.position.y + move.y * moveDelta,
                    }
                    moved = true
                }
            }
        }

        // Animate entities
        const step = (deltaTime * this.view.options.tick_per_second) / 1000
        this.entities.forEach(entity => entity.stepAnimations(step))

        if (moved || this.needFullRender) {
            const area = this.view.area(1.2)
            this.entities.forEach((e, id) => {
                const p = e.animatedPos
                if (!(p.x >= area.min.x && p.x <= area.max.x &&
                      p.y >= area.min.y && p.y <= area.max.y))
                    this.entities.delete(id)
            })
            this.stream.move(this.view.area(1.1))
            this.view.clearStatic()

            const staticDelay = Math.max(30 / this.view.options.tick_per_second, 3) / 1000
            const freezeTime = Date.now() - staticDelay
            this.entities.forEach(entity => {
                if (entity.freeze(freezeTime)) {
                    entity.draw(this.view, false)
                }
            })
            this.needFullRender = false
        }

        this.entities.forEach(entity => {
            if (!entity.freezed) {
                entity.draw(this.view, true)
            }
        })
    }

    private keyboardListener(event: KeyboardEvent) {
        this.pressedKeys[event.code] = event.type === "keydown";
    }

    private getEntity(id: string) {
        return new Promise<Entity>((resolve, reject) => {
            if (this.entities.has(id)) {
                resolve(this.entities.get(id)!)
            } else {
                if (this.pendingEntities.has(id)) {
                    this.pendingEntities.get(id)!.resolve.push(resolve)
                    this.pendingEntities.get(id)!.reject.push(reject)
                } else {
                    const query: EntityQuery = {
                        resolve: [resolve], reject: [reject],
                        fail: setTimeout(() => {
                            clearTimeout(query.retry!)
                            this.pendingEntities.delete(id)
                            query.reject.forEach(rej => rej(`Cannot get entity ${id}: timeout`))
                        }, 30000)
                    }
                    const tryIt = () => {
                        this.stream.entity(id)
                        query.retry = setTimeout(tryIt, 5000)
                    }; tryIt()
                    this.pendingEntities.set(id, query)
                }
            }
        })
    }
    private gotEntity(entt: FullEntity) {
        if (this.entities.has(entt.id)) {
            const it = this.entities.get(entt.id)!
            if (it.animatePos(entt.position))
                this.needFullRender = true
            if (entt.rotation) {
                if (it.animateRot(toRad(entt.rotation)))
                    this.needFullRender = true
            }
        } else {
            const entity = new Entity(entt.type, entt.position, entt.rotation)
            this.entities.set(entt.id, entity)
            if (this.pendingEntities.has(entt.id)) {
                const query = this.pendingEntities.get(entt.id)!
                clearTimeout(query.fail)
                clearTimeout(query.retry!)
                query.resolve.forEach(res => res(entity))
                this.pendingEntities.delete(entt.id)
            }
        }
    }

    private eventListener(event: Event) {
        switch (event.type) {
            case EventType.Protocol:
                if (event.id == 'connect') {
                    this.view.scale = this.view.scale * 1
                } else {
                    console.warn(event.data)
                }
                break;

            case EventType.Entities:
                for (const entt of event.data) {
                    this.gotEntity(entt)
                }
                break

            case EventType.Tick:
                break;

            case EventType.Move:
                this.getEntity(event.data.id).then(entity => {
                    if (entity.animatePos(event.data.position))
                        this.needFullRender = true
                })
                break;

            case EventType.Rotate:
                this.getEntity(event.data.id).then(entity => {
                    if (entity.animateRot(event.data.rotation))
                        this.needFullRender = true
                })
                break;

            default:
                console.warn(event)
        }
    }
}