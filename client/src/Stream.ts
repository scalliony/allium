import { Query, Area, QueryKind, Event, EventType, RoundArea } from './common'
import { Throttler, throttle } from './throttle'

type Handler = (ev: Event) => void
export default class Stream {
    private socket: WebSocket
    private reconnectAttempts = 0
    private handler: Handler
    private moveLimit: Throttler<[Area]>

    constructor(on: Handler) {
        this.moveLimit = throttle(area => this.send({kind: QueryKind.Move, data: area}), 500, { start: true, middle: true })
        this.handler = on
        this.socket = this.connect()
    }

    private connect() {
        this.reconnectAttempts++
        const url = `ws${window.location.protocol == 'https:' ? 's' : ''}://${window.location.host}/api/ws`
        const socket = new WebSocket(url)
        this.socket = socket
        const timeout = setTimeout(() => socket.close(), 5000)
        socket.onopen = () => {
            clearTimeout(timeout)
            this.reconnectAttempts = 0
            this.handler({ id: 'connect', type: EventType.Protocol, data: null })
        }
        socket.onclose = ev => {
            if (ev.code != 42 || ev.reason != 'force-close') {
                setTimeout(() => this.connect(),
                    (this.reconnectAttempts > 50 ? 50 : this.reconnectAttempts) * 100)
            }
        }
        socket.onmessage = ev => {
            if ((ev.data as string).charAt(0) == '{') {
                this.handler(JSON.parse(ev.data) as Event)
            } else {
                this.handler({ id: '', type: EventType.Protocol, data: ev.data })
            }
        }
        socket.onerror = err => console.error('WS error', err)
        return socket
    }

    send(query: Query) {
        try {
            this.socket.send(JSON.stringify(query))
        } catch(_) { }
    }
    move(to: Area) {
        this.moveLimit(RoundArea(to))
    }
    entity(id: string) {
        this.send({kind: QueryKind.Entity, data: id})
    }

    close() {
        this.socket.close(42, 'force-close')
    }
}