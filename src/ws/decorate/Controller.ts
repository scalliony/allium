import { getController } from "./types"

interface ControllerOptions { baseRoute?: string, json?: boolean, socket?: boolean }
export function Controller(options?: ControllerOptions) {
    return (target: Function) => {
        const data = getController(target.prototype)
        data.baseRoute = options?.baseRoute
        data.json = options?.json ?? false
        data.socket = options?.socket ?? false
        data.ok = true
    }
}
export function JsonController(baseRoute?: string) {
    return Controller({ baseRoute, json: true })
}
export function SocketController(baseRoute?: string) {
    return Controller({ baseRoute, socket: true })
}