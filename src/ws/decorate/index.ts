import { Logger } from 'pino';
import statuses = require('statuses');
import { HttpRequest, HttpResponse, RecognizedString, TemplatedApp, us_socket_context_t, WebSocket } from 'uWebSockets.js'
import { HttpError, HttpInternalError } from './HttpError';
import safetyPatchRes from './safetyPatchRes';
import { getController, HttpContext, isPromise } from './types';

export * from './Controller'
export * from './Route'
export * from './Param'
export * from './types'
export * from './HttpError'

interface RoutingOptions {
    logger?: Logger|undefined
    controllers: (Function|[Function, any])[]
    routePrefix?: string|undefined
    production?: boolean
}
export function registerControllers(app: TemplatedApp, options: RoutingOptions) {
    options.controllers.forEach(ctrl => {
        const type = Array.isArray(ctrl) ? ctrl[0] : ctrl
        const routing = getController(type.prototype)
        if (!routing.ok) {
            throw new Error(`${type.name} is not a valid @Controller`)
        }
        const instance: { [method: string]: Function } = Array.isArray(ctrl) ? ctrl[1] : new (ctrl as any)()
        if (routing.socket) {
            routing.socketHandlers.forEach(handler => {
                if (!handler.ok) {
                    throw new Error(`${type.name}.${handler.targetKey} is not a valid @OnSocket method`)
                }
                const run = function (bonus: { [key: string]: any }) {
                    try {
                        handler.target.bind(instance, ...handler.params.map(param => {
                            switch (param) {
                                case 'logger':
                                    return options.logger

                                default:
                                    return bonus[param]
                            }
                        }))()
                    } catch (error) {
                        (options.logger??console).warn('WS error', JSON.stringify(error))
                    }
                }
                instance[handler.kind] = (() => {
                    switch (handler.kind) {
                        case 'open':
                        case 'drain':
                        case 'ping':
                        case 'pong':
                            return (ws: WebSocket) => run({ ws })

                        case 'close':
                            return (ws: WebSocket, code: number, message: ArrayBuffer) => run({
                                ws, 'close-code': code, message: { data: message, isBinary: false }
                            })
                        case 'message':
                            return (ws: WebSocket, message: ArrayBuffer, isBinary: boolean) => run({
                                ws, message: { data: message, isBinary }
                            })
                        case 'upgrade':
                            return (res: HttpResponse, req: HttpRequest, context: us_socket_context_t) => run({
                                response: res, request: req, context
                            })
                    }
                })()
            })
            const route = (options.routePrefix ?? '') + (routing.baseRoute ?? '/*')
            app.ws(route, instance)
        } else if (routing.socketHandlers.length > 0) {
            throw new Error(`Only @SocketController can use @OnSocket, ${type.name} is not`);
        }
        routing.methods.forEach(method => {
            if (!method.ok) {
                throw new Error(`${type.name}.${method.targetKey} is not a valid @Route method`)
            }

            //TODO: middlewares
            const route = (options.routePrefix ?? '') + (routing.baseRoute ?? '') + method.route
            instance[method.targetKey] = method.target.bind(instance)
            const handler = (res: HttpResponse, req: HttpRequest) => {
                safetyPatchRes(res)

                const writeStatus = (code: number) => {
                    res.writeStatus(`${code} ${statuses(code)}`)
                }
                const writeData = (data: any) => {
                    if (routing.json) {
                        res.writeHeader('Content-Type', method.contentType ?? 'application/json')
                        res.write(JSON.stringify(data))
                    } else {
                        res.writeHeader('Content-Type', method.contentType ?? 'text/html')
                        if (typeof data === 'string') {
                            res.write(data)
                        } else {
                            (options.logger ?? console).error('Can not format output of type ' + typeof data, data)
                            throw new HttpInternalError('Can not format output of type ' + typeof data)
                        }
                    }
                }
                const handleError = (e: any) => {
                    if (typeof e === 'string') {
                        writeStatus(500)
                        writeData(routing.json ? { error: true, kind: 'error', message: e, code: 500 } : e)
                    } else if (e instanceof HttpError) {
                        writeStatus(e.code)
                        let out: any = 'error'
                        if (routing.json) {
                            out = { error: true, kind: 'error', message: e.message, code: e.code };
                            if (!options.production) {
                                out.stack = e.stack
                                out.name = e.name
                            }
                        } else {
                            out = `Error ${e.code} ${statuses(e.code)}: ${e.message}`
                            if (!options.production) {
                                out = `<pre>${out} (${e.name})\n${e.stack}</pre>`
                            }
                        }
                        writeData(out)
                    } else if (e instanceof Error) {
                        handleError(HttpError.Of(500, e))
                    } else {
                        (options.logger ?? console).error('Can not format error of type ' + typeof e, e)
                        handleError(new HttpInternalError('Can not format error of type ' + typeof e))
                    }
                }
                const handleData: (out: any) => boolean|PromiseLike<any> = (out: any) => {
                    if (out === res) {
                        return false
                    }
                    if (isPromise(out)) {
                        return out.then(out => {
                            handleData(out)
                            if (out !== res)
                                res.end()
                        }, e => handleError(e))
                    }
                    writeStatus(method.successCode)
                    //TODO: write headers
                    writeData(out)
                    return true
                }

                try {
                    const out = handleData(instance[method.targetKey](...method.params.map(param => {
                        switch (param) {
                            case 'request':
                                return req
                            case 'response':
                                return res
                            case 'context':
                                return { req, res } as HttpContext
                            case 'logger':
                                return options.logger
                        }
                    })))
                    if (out === false)
                        return;
                    if (out !== true)
                        return out;
                } catch (e) {
                    handleError(e)
                }
                res.end()
            }
            type EasyApp = { [method: string]: (pattern: RecognizedString, handler: (res: HttpResponse, req: HttpRequest) => void) => void };
            (app as unknown as EasyApp)[method.kind](route, handler)
        })
    });
}