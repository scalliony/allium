import { getMethod, HttpMethod } from "./types"

interface RouteOptions {
    successCode?: number
    contentType?: string
}
export function Route(method: HttpMethod, route: string, options?: RouteOptions) {
    return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<Function>) => {
        const data = getMethod(target, propertyKey.toString())
        data.kind = method,
        data.route = route
        data.target = descriptor.value,
        data.targetKey = propertyKey.toString()
        data.successCode = options?.successCode ?? 200
        data.contentType = options?.contentType
        data.ok = true
    }
}
export function Get(route: string, options?: RouteOptions) {
    return Route('get', route, options)
}