import { getMethod, HttpParameterType } from "./types"

export function Param(type: HttpParameterType) {
    return (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
        const params = getMethod(target, propertyKey.toString()).params
        if (parameterIndex >= params.length)
            params.length = parameterIndex + 1

        params[parameterIndex] = type
    }
}
export function Ctx() {
    return Param('context')
}
export function Res() {
    return Param('response')
}
export function Req() {
    return Param('request')
}
export function Log() {
    return Param('request')
}