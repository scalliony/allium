import { getSocketHandler, SocketMethod, SocketParameterType } from "./types"
export { SocketController } from './Controller'
export { SocketMessage } from './types'

export function OnSocket(method: SocketMethod) {
    return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<Function>) => {
        const data = getSocketHandler(target, propertyKey.toString())
        data.kind = method,
        data.target = descriptor.value,
        data.targetKey = propertyKey.toString()
        data.ok = true
    }
}
export function OnMessage() {
    return OnSocket('message')
}
export function OnOpen() {
    return OnSocket('open')
}
export function OnClose() {
    return OnSocket('close')
}
export function OnDrain() {
    return OnSocket('drain')
}
export function Upgrade() {
    return OnSocket('upgrade')
}

export function Param(type: SocketParameterType) {
    return (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
        const params = getSocketHandler(target, propertyKey.toString()).params
        if (parameterIndex >= params.length)
            params.length = parameterIndex + 1

        params[parameterIndex] = type
    }
}
export function Log() {
    return Param('logger')
}
export function WS() {
    return Param('ws')
}
export function Message() {
    return Param('message')
}