export class HttpError extends Error {
    constructor(public code: number, m: string) {
        super(m);
        this.name = 'HttpError'
        Object.setPrototypeOf(this, HttpError.prototype);
    }

    static Of(code: number, e: Error) {
        Object.setPrototypeOf(e, HttpError.prototype)
        const he = e as HttpError
        he.code = code
        return he
    }
}
export class HttpNotFoundError extends HttpError {
    constructor(m: string) {
        super(404, m);
    }
}
export class HttpForbiddenError extends HttpError {
    constructor(m: string) {
        super(403, m);;
    }
}
export class HttpInternalError extends HttpError {
    constructor(m: string) {
        super(500, m);
    }
}