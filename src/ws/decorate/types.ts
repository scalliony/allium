import { HttpRequest, HttpResponse } from "uWebSockets.js";

export type HttpMethod = 'get' | 'del' | 'post' | 'put' | 'patch' | 'head' | 'options' | 'connect' | 'trace' | 'any'
export type HttpParameterType = 'context' | 'request' | 'response' | 'logger'

export type SocketMethod = 'upgrade' | 'open' | 'message' | 'drain' | 'close' | 'ping' | 'pong'
export type SocketParameterType = HttpParameterType | 'ws' | 'message' | 'close-code'

export interface Method<P> {
    target: Function
    targetKey: string
    params: (P | undefined)[]
    ok: boolean
}
export interface RoutingMethod extends Method<HttpParameterType> {
    kind: HttpMethod
    route: string
    successCode: number
    contentType: string|undefined
}

export interface SocketHandler extends Method<SocketParameterType> {
    kind: SocketMethod
}

export interface RoutingController {
    baseRoute: string
    json: boolean
    socket: boolean
    methods: RoutingMethod[]
    socketHandlers: SocketHandler[]
    ok: boolean
}

export interface RoutingSocket {
    route: string

}

export function getController(target: any) {
    if (!('__routing' in target))
        target.__routing = { methods: [], socketHandlers: [], ok: false }

    return target.__routing as RoutingController
}
export function getMethod(target: any, name: string) {
    const data = getController(target)
    let method: any = data.methods.find(m => m.targetKey == name)
    if (method === undefined) {
        method = { targetKey: name, ok: false, params: [] }
        data.methods.push(method)
    }
    return method as RoutingMethod
}
export function getSocketHandler(target: any, name: string) {
    const data = getController(target)
    let handler: any = data.socketHandlers.find(m => m.targetKey == name)
    if (handler === undefined) {
        handler = { targetKey: name, ok: false, params: [] }
        data.socketHandlers.push(handler)
    }
    return handler as SocketHandler
}

export function isPromise<T, S>(obj: PromiseLike<T>|S): obj is PromiseLike<T> {
    return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof (obj as any).then === 'function';
}

export interface HttpContext {
    req: HttpRequest
    res: HttpResponse
}
export interface SocketMessage {
    data: ArrayBuffer
    isBinary: boolean
}

declare type PropertyDecorator = (target: Object, propertyKey: string | symbol) => void;