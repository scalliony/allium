import { Logger } from "pino";
import { DEDICATED_COMPRESSOR_64KB, WebSocket } from "uWebSockets.js";
import { Area, EventType, Query, QueryKind } from '@common'
import { Entity, Event } from "../../sql";
import { Listener } from "../../utils/types";
import { Log, Message, OnClose, OnMessage, OnOpen, WS, SocketController, SocketMessage } from "../decorate/Socket";

type OpenSocket = WebSocket & { handler: (e: Event) => void, view?: Area }

@SocketController('/ws')
export class StreamController {
    compression = DEDICATED_COMPRESSOR_64KB
    constructor(private listener: Listener<Event>) { }

    @OnOpen()
    open(@WS() ws: OpenSocket): void {
        ws['handler'] = (e: Event) => {
            if (!ws['handler'])
                return
            //FIXME: drain and backpresure
            //TODO: if in range and rights
            ws.send(JSON.stringify(e), false, true)
        }
        this.listener.add(ws['handler'])
        ws.send('Server: Hi')
    }

    @OnMessage()
    message(@WS() ws: OpenSocket, @Message() message: SocketMessage, @Log() log: Logger): void {
        if (!message.isBinary) {
            try {
                const query: Query = JSON.parse(Buffer.from(message.data).toString())
                switch (query.kind) {
                    case QueryKind.Move:
                        //MAYBE: assert area size limit
                        ('view' in ws ?
                            Entity.InRangeDiff(query.data, ws['view']) :
                            Entity.InRange(query.data)).then(data => ws.send(
                        JSON.stringify({ type: EventType.Entities, data }), false, true))
                        ws['view'] = query.data
                        break;

                    case QueryKind.Entity:
                        Entity.ById(query.data).then(entt => ws.send(JSON.stringify({
                            type: EventType.Entities, data: [entt]
                        }), false, true))
                        break;

                    default:
                        throw new Error(`Bad query ${Buffer.from(message.data).toString()}`);
                }
            } catch (e) {
                log.error('OnMessage', e)
            }
        } else
            log.info('binary message')
    }

    @OnClose()
    close(@WS() ws: OpenSocket): void {
        this.listener.remove(ws['handler'])
        ws['handler'] = undefined
    }
}