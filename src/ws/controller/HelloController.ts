import { JsonController, Route } from "../decorate";

@JsonController('/hello')
export abstract class HelloController {

    @Route('get', '/test')
    test() {
        return { int: 42, texts: ['a', 'bcd'] }
    }

    @Route('get', '/a')
    a() {
        return 'a'
    }

    @Route('get', '/async')
    async promise() {
        await new Promise((resolve) => setTimeout(resolve, 3000))
        return 'async'
    }

}