import * as fs from 'fs'
import * as mime from 'mime-types'
import * as path from 'path'
import * as zlib from 'zlib'
import { HttpRequest, HttpResponse } from "uWebSockets.js";
import { PROD } from "../../utils/prod";
import { Controller, Get, HttpNotFoundError, Req, Res } from "../decorate";
import pipeStreamOverResponse from '../pipeStreamOverResponse';

const compressibleSet = new Set(['.ico', '.js', '.json', '.html'])
class File {
    mtime: string
    size: number
    pathname: string
    type: string
    file?: Buffer
    /** Only if PROD */
    brotliFile?: Buffer

    constructor(pathname: string, cacheFile: boolean) {
        this.pathname = pathname
        const { mtime, size } = fs.statSync(pathname)
        this.mtime = mtime.toUTCString()
        this.size = size
        const ext = path.extname(pathname).slice(1)
        this.type = mime.types[ext]
        if (cacheFile) {
            this.file = fs.readFileSync(pathname)
            if (PROD && compressibleSet.has(path.extname(pathname))) {
                this.brotliFile = (zlib as any).brotliCompressSync(this.file)
            }
        }
    }
}

@Controller()
export abstract class StaticController {

    private pathnames: { [filename: string]: string } = {}
    private cache: { [filename: string]: File } = {}
    private index: string = '/index.html'
    constructor() {
        const root = process.env.CLIENT_DIR ?? (__dirname + '/client')
        try {
            if (!fs.existsSync(root)) {
                fs.mkdirSync(root)
            }
            this.loadFilesRec(root, '/')
        } catch (e) {
            console.error(e)
        }
    }

    @Get('/*')
    serve(@Req() req: HttpRequest, @Res() res: HttpResponse) {
        const meta = this.getMeta(req.getUrl())
        if (!meta) throw new HttpNotFoundError('File not found');

        const {size, pathname, brotliFile, file, type} = meta
        if (file) {
            res.cork(() => {
                res.writeHeader('Content-Type', type)
                if (PROD && brotliFile) {
                    return res.writeHeader('Content-Encoding', 'br').end(brotliFile)
                } else {
                    return res.end(file)
                }
            })
        }
        res.writeHeader('Content-Type', type)
        const readStream = fs.createReadStream(pathname)
        return pipeStreamOverResponse(res, readStream, size)
    }

    private getMeta(filename: string): File | false {
        if (filename == '/')
            filename = this.index
        const existingMeta = this.cache[filename]
        if (existingMeta) return existingMeta
        const pathname = this.pathnames[filename]
        if (!pathname) return false
        return (this.cache[filename] = new File(pathname, this.mustCache(filename)))
    }
    private mustCache(filename: string): boolean {
        return PROD
    }
    private loadFilesRec(root: string, prefix: string) {
        fs.readdirSync(root).forEach((dirent) => {
            const dirpath = path.join(root, dirent)
            const stat = fs.statSync(dirpath)
            if (stat.isFile()) {
                const key = prefix + dirent
                this.pathnames[key] = dirpath
            } else if (stat.isDirectory()) {
                this.loadFilesRec(dirpath, `${prefix}${dirent}/`)
            }
        })
    }
}