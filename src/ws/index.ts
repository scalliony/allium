import { Logger } from 'pino'
import { App } from 'uWebSockets.js'
import { Listener } from '../utils/types'
import { Event } from '../sql/Event'
import { PROD } from '../utils/prod'
import { registerControllers } from './decorate'

import { StreamController } from './controller/StreamController'
import { HelloController } from './controller/HelloController'
import { StaticController } from './controller/StaticController'

export default function (listener: Listener<Event>, logger: Logger) {
    const app = App({ /*TODO: SSLApp */ })

    registerControllers(app, {
        controllers: [StaticController],
        production: PROD
    })
    registerControllers(app, {
        routePrefix: '/api',
        logger,
        controllers: [HelloController, [StreamController, new StreamController(listener)]],
        production: PROD
    })

    app.listen(3000, () => logger.info('Listen on *:3000'))
}
