import * as pino from 'pino'

/** Provide same config than SPDLOG_LEVEL with environment variable LOG_LEVEL */
let top_level: string = 'info'
const child_levels = new Map<string, string>()
function load() {
    if ('LOG_LEVEL' in process.env) {
        for (const field of process.env.LOG_LEVEL.split(',')) {
            if (field.includes('=')) {
                const [key, val] = field.split('=', 2)
                child_levels.set(key, val)
            } else {
                top_level = field
            }
        }
    }
}
load()

const root = pino({ level: top_level })
root.trace('Logger created')

export function child(name: string) {
    const l = root.child({ name, level: child_levels.get(name) })
    l.trace('Logger created')
    return l
}

export default root
