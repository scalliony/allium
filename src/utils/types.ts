type Listen<T> = (handle: (id: T) => void) => void
export interface Listener<T> { add: Listen<T>, remove: Listen<T> }