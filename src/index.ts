import logger, { child as subLogger } from './utils/logger'
import connectDB from './sql'
import runWS from './ws'

async function run() {
    if (!('DB_URI' in process.env))
        throw `Usage: DB_URI=<postgresql://...> ${process.argv[0]} ${process.argv[1]}`

    logger.warn('Allium ' + (process.env.npm_package_version ?? ''))
    const { pool, listener } = await connectDB(process.env.DB_URI, subLogger('db'))
    runWS(listener, subLogger('ws'))
}
run()
