import { Logger } from 'pino'
import { Pool, QueryResult } from 'pg'
import createSubscriber from 'pg-listen'
import { Listener } from '../utils/types'
import { Event } from './Event'
export * from './Event'
export * from './Entity'

export function oneRow<T>(rows: QueryResult<T>) {
    if (rows.rowCount != 1)
        throw 'expect a single row got ' + rows.rowCount
    return rows.rows[0]
}

let pool: Pool
export function getDB() { return pool }

export default async function(url: string, log: Logger): Promise<{pool: Pool, listener: Listener<Event>}> {
    try {
        pool = new Pool({
            connectionString: url,
            min: 1, max: 10
        })
        const subscriber = createSubscriber({
            connectionString: url
        }, {
            parse: (s: string) => s,
            serialize: (d: any) => d.toString()
        })
        await subscriber.connect()
        subscriber.events.on('error', error => log.error(error))
        subscriber.events.on('reconnect', () => log.debug('reconnecting subscriber'))
        await subscriber.listenTo('event')
        log.info('Connected')
        process.on("exit", () => {
            subscriber.close()
            pool.end()
        })
        const listener: Listener<Event> = {
            add: (handle: (event: Event) => void) =>
                subscriber.notifications.on('event', id => Event.ById(id)
                    .then(handle).catch(err => log.error(err))),
            remove: (handle: (event: Event) => void) =>
                subscriber.notifications.removeListener('event', handle)
        }
        return { pool, listener }
    } catch (error) {
        log.fatal('Connection fail', error)
        throw error
    }
}