import { QueryConfig } from "pg"
import { getDB, oneRow } from "."
import { AnyEvent, EventType } from '@common'

export class Event implements AnyEvent {

    id: string
    type: EventType
    data: object

    static ByIdQuery(id: string): QueryConfig {
        return {
            name: 'event',
            text: 'SELECT id, type, data FROM event WHERE id = $1',
            values: [id]
        }
    }
    static ById(id: string) {
        return getDB().query<Event>(Event.ByIdQuery(id)).then(oneRow)
    }

}