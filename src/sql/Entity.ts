import { getDB, oneRow } from "."
import { Area, Cell, EntityType, FullEntity as Interface, Rotation, RoundArea } from '@common'

type EntityRow = Entity & { x: number, y: number }
export class Entity implements Interface {

    id: string
    type: EntityType
    position: Cell
    rotation?: Rotation

    static FromRow(row: EntityRow): Entity {
        row.position = { x: row.x, y: row.y }
        delete row.x
        delete row.y
        return row
    }

    static ById(id: string) {
        return getDB().query<EntityRow>({
            name: 'entity',
            text:
`SELECT entity.id as "id", type, MIN(cell.x) as "x", MIN(cell.y) as "y", rotation
FROM entity JOIN cell ON entity_id = entity.id LEFT JOIN bot ON bot.id = entity.id
WHERE entity.id = $1 GROUP BY entity.id, type, rotation`,
            values: [id]
        }).then(oneRow).then(Entity.FromRow)
    }

    static InRange(a: Area) {
        const { min, max } = RoundArea(a)
        return getDB().query<EntityRow>({
            name: 'entity-range',
            text:
`SELECT entity.id as "id", type, MIN(cell.x) as "x", MIN(cell.y) as "y", rotation
FROM entity JOIN cell ON entity_id = entity.id AND x >= $1 AND x <= $2 AND y >= $3 AND y <= $4
LEFT JOIN bot ON bot.id = entity.id GROUP BY entity.id, type, rotation`,
            values: [min.x, max.x, min.y, max.y]
        }).then(res => res.rows.map(Entity.FromRow))
    }

    static InRangeDiff(a: Area, minus: Area) {
        const { min, max } = RoundArea(a)
        const not = RoundArea(minus)
        return getDB().query<EntityRow>({
            name: 'entity-range-diff',
            text:
`SELECT entity.id as "id", type, MIN(cell.x) as "x", MIN(cell.y) as "y", rotation
FROM entity JOIN cell ON entity_id = entity.id AND x >= $1 AND x <= $2 AND y >= $3 AND y <= $4
AND NOT (x >= $5 AND x <= $6 AND y >= $7 AND y <= $8)
LEFT JOIN bot ON bot.id = entity.id GROUP BY entity.id, type, rotation`,
            values: [min.x, max.x, min.y, max.y, not.min.x, not.max.x, not.min.y, not.max.y]
        }).then(res => res.rows.map(Entity.FromRow))
    }
}