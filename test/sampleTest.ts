import { add } from '../src/utils/sample'

import { expect } from 'chai'
import { describe, it } from 'mocha'

describe('My sample file', () => {

    it('should be able to add things correctly', () => {
        expect(add(3, 4)).to.equal(7)
    })

})
