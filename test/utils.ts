import 'reflect-metadata'

import * as chai from 'chai'
import * as chaiPromised from 'chai-as-promised'
import { Server } from 'http'
import * as Mocha from 'mocha'
import * as logger from 'pino'
import { Connection } from 'typeorm'
import connectAPI from '../src/controller'
import connectDB from '../src/entity'
import { Listen } from '../src/utils/types'

chai.use(chaiPromised)
const TEST_DB = 'postgresql://scalliony@localhost/scalliony'

export const useDB = (fn: (this: Mocha.Suite, listen: Listen) => void) => () => {
    let pair: { orm: Connection, listen: Listen }
    before(async () => { pair = await connectDB(process.env.DB_URI ?? TEST_DB, logger()) })

    fn.bind(this)(pair.listen)

    after(async () => await pair.orm.close())
}

export const useAPI = (fn: (this: Mocha.Suite, api: Server) => void, listen: Listen = () => {}) => () => {
    const server = connectAPI(listen, logger())

    fn.bind(this)(server)

    after(() => server.close())
}
