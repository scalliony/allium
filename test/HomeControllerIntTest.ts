import * as request from 'supertest'
import { useAPI } from './utils'

describe('HomeController on express', useAPI(server => {

    it('should respond on /api', done => {
        request(server).get('/api')
            .expect(200, done)
    })

    it('should respond on /api/:id', done => {
        const id = 42
        request(server).get('/api/' + id)
            .expect(200, 'This action returns user #42', done)
    })

    it('should respond index on /', done => {
        request(server).get('/')
            .expect(200, done)
    })

    it('should 404 everywhere else', done => {
        request(server).get('/bar')
            .expect(404, done)
    })

}))
