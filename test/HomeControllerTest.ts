import { HomeController } from '../src/controller/HomeController'

import { expect } from 'chai'
import { describe, it } from 'mocha'

describe('HomeController', () => {

    const controller = new HomeController()

    it('should respond on getAll', () => {
        expect(controller.getAll()).deep.equal('This action returns all users')
    })

    it('should respond on getOne', () => {
        const id = 42
        expect(controller.getOne(id)).deep.equal('This action returns user #42')
    })

})
