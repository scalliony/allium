FROM node AS builder
ARG COMMIT=main
RUN git clone https://framagit.org/scalliony/allium.git
WORKDIR /allium/client
RUN git checkout ${COMMIT}
RUN yarn install && yarn run build
WORKDIR /allium
RUN yarn install && yarn run build
RUN mv ./client/dist ./build/client && rm -rf node_modules
RUN yarn install --prod

FROM node
COPY --from=builder /allium/build .
COPY --from=builder /allium/node_modules node_modules
ENV NODE_ENV=production
CMD [ "node", "./index.js" ]
EXPOSE 3000/tcp
